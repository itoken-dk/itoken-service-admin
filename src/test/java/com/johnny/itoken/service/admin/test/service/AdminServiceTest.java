package com.johnny.itoken.service.admin.test.service;

import com.johnny.itoken.service.admin.ServiceAdminApplication;
import com.johnny.itoken.service.admin.domain.TbSysUser;
import com.johnny.itoken.service.admin.mapper.TbSysUserMapper;
import com.johnny.itoken.service.admin.service.AdminService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@Transactional
@Rollback
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServiceAdminApplication.class)
@ActiveProfiles(value = "prod")
public class AdminServiceTest {
    @Autowired
    private AdminService adminService;

    @Test
    public void register() {
        TbSysUser user = new TbSysUser();
        user.setUserCode(UUID.randomUUID().toString());
        user.setLoginCode("johnny@qq.com");
        user.setUserName("johnny");
        user.setPassword("123456");
        user.setUserType("0");
        user.setMgrType("1");
        user.setStatus("0");
        user.setCreateDate(new Date());
        user.setCreateBy(user.getUserCode());
        user.setUpdateDate(new Date());
        user.setUpdateBy(user.getUserCode());
        user.setCorpCode("0");
        user.setCorpName("itoken");
        adminService.register(user);
    }

    @Test
    public void login() {
        TbSysUser user = adminService.login("johnny@qq.com", "123456");
        Assert.assertNotNull(user);
    }


}

package com.johnny.itoken.service.admin.service;

import com.johnny.itoken.service.admin.domain.TbSysUser;

public interface AdminService {
    public void register(TbSysUser user);

    public TbSysUser login(String loginCode, String plantPassword);
}

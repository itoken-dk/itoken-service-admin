package com.johnny.itoken.service.admin.mapper;

import com.johnny.itoken.service.admin.domain.TbSysUser;
import tk.mybatis.mapper.MyMapper;

public interface TbSysUserMapper extends MyMapper<TbSysUser> {
}